<!DOCTYPE html>
<html lang=en>
<head>
	<meta charset="utf-8">

	<title>Obfuscated Apples | pseudorandom</title>

	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@sarahjamielewis" />
	<meta name="twitter:creator" content="@sarahjamielewis" />
	<meta property="og:url" content="https://pseudorandom.resistant.tech/obfuscated_apples.html" />
	<meta property="og:description" content="Generating noise in a way which is indistinguishable from real signal is a ridiculously hard problem. Obfuscation does  not hide signal, it only adds noise" />
	<meta property="og:title" content="Obfuscated Apples" />
	<meta name="twitter:image" content="https://pseudorandom.resistant.tech/obfuscated_apples.png">

	<link rel="alternate" type="application/atom+xml" href="/feed.xml" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="styles.css">

	  <link rel="stylesheet" href="/katex/katex.min.css" integrity="sha384-RZU/ijkSsFbcmivfdRBQDtwuwVqK7GMOw6IMvKyeWL2K5UAlyp6WonmB8m7Jd0Hn" crossorigin="anonymous">

    <!-- The loading of KaTeX is deferred to speed up page rendering -->
    <script defer src="/katex//katex.min.js" integrity="sha384-pK1WpvzWVBQiP0/GjnvRxV4mOb0oxFuyRxJlk6vVw146n3egcN5C925NCP7a7BY8" crossorigin="anonymous"></script>

    <!-- To automatically render math in text elements, include the auto-render extension: -->
    <script defer src="/katex/auto-render.min.js" integrity="sha384-vZTG03m+2yp6N6BNi5iM4rW4oIwk5DfcNdFfxkk9ZWpDriOkXX8voJBFrAO7MpVl" crossorigin="anonymous"
        onload="renderMathInElement(document.body);"></script>
</head>

<body>
<header>
<nav>
	<strong>pseudorandom</strong>
	<a href="./index.html">home</a>
	<a href="mailto:sarah@openprivacy.ca">email</a>
	<a href="cwtch:icyt7rvdsdci42h6si2ibtwucdmjrlcb2ezkecuagtquiiflbkxf2cqd">cwtch</a>
	<a href="/feed.xml">atom</a>
</nav>
</header>
<article>

<h1 id="obfuscated-apples">Obfuscated Apples</h1>
<p>Generating noise in a way which is indistinguishable from real signal is a ridiculously hard problem. Obfuscation does not hide signal, it only adds noise<em class="footnotelabel"></em>.</p>
<p class="sidenote">
if you take anything away from this article please let it be this fact.
</p>
<p>Sadly, most people operate under the assumption that adding noise to a system is all that it takes to make the signal unrecoverable. This logic is very clearly in operation in Apple’s new proposal for on-device scanning<em class="footnotelabel"></em> <a class="sidenote" href="https://www.apple.com/child-safety/pdf/CSAM_Detection_Technical_Summary.pdf">technical summary</a> which, among other things, proposes generating <em>synthetic</em> matches to hide the true number of <em>real</em> matches in the system.</p>
I want to take this opportunity to break down how this kind of obfuscation can be defeated even when not considering the fact that it is <strong>Apple themselves who are charged with generating and maintaining the safety parameters of the system</strong><em class="footnotelabel"></em>.
<p class="sidenote">
i.e. even if we treat the people who design and build this system as honest adversaries.
</p>
<h2 id="sketching-a-basic-scheme">Sketching a Basic Scheme</h2>
<p>For the sake of clarity I will omit the technical details of the private set intersection protocol, and the threshold scheme, and we will operate under the assumption that both are cryptographically secure. We will also assume that the database of images to compare is <em>incorruptible</em><em class="footnotelabel"></em> <span class="sidenote">This is clearly not the case.</span>.</p>
<p>At the heart of system is a (mostly) black box that contains a <strong>perceptual</strong> hash function that analyzes an image and spits out a hash, this hash is then compared against a database of known hashes and if a match is found the system reports <code>true</code> and otherwise reports <code>false</code><em class="footnotelabel"></em>. <span class="sidenote">As we will see later on, perceptual hashes are <strong>not</strong> cryptographic hashes.<span></p>
<p>Throughout this article I will use the term <strong>match</strong> when talking about both true and false positives, though I will mostly assume any matches are false positives.</p>
<p>According to documentation provided by Apple, the server learns any matches occurred on the phone during the PSI protocol.<em class="footnotelabel"></em></p>
<blockquote class="sidenote">
“The output of PSI protocol on the server reveals whether there is a match or not” - Apple Technical Summary
</blockquote>
<p>When a certain threshold of matches are reached, the server gains the ability to decrypt all data associated , a human reviews that data, and a determination is made.</p>
<p>As presented the system above has one major flaw<em class="footnotelabel"></em> <span class="sidenote">(besides the gross nature of co-opting a personal device as a surveillance system)</span>: the server learns how many matches the device has reported prior to being able to decrypt those matches.</p>
<p>This is obviously very important metadata in the context of the system and as such needs to be protected - if it is not then Apple, or someone who can compel Apple to release the data, can identify potential targets based on this metadata.<em class="footnotelabel"></em> <span class="sidenote">As we shall soon discuss targeting people in way would be highly irrational if your goal was to actually hunt people doing harm, but people are not rational actors.</span></p>
<p>To protect this data Apple relies on the invocation of so-called “Synthetic Vouchers” with the following property:</p>
<blockquote>
“The probability that a device uploads a synthetic voucher instead of a real voucher for an image is calibrated to ensure the total number of synthetics is of the same order of magnitude as the threshold”
</blockquote>
<h2 id="deriving-probabilities">Deriving Probabilities</h2>
<p>So, that is it right? Problem averted? Let’s not be too hasty…</p>
<p>From what we know so far there are a few interesting parameters in this system that Apple must determine values for.</p>
<p>There is the threshold <span class="math inline"><em>t</em></span> of matches necessary to decrypt the data, there is the probability of a device generating a synthetic match <span class="math inline"><em>P</em>(<code>synthetic</code>)</span> and there is the probability of a false positive match <span class="math inline"><em>P</em>(<code>falsepositive</code>)</span>.</p>
<p>We also know that Apple has constructed these parameters such that the probability of an account being flagged for human review (i.e. when number of matches <span class="math inline"><em>M</em> &gt; <em>t</em></span> is <span class="math inline"><em>P</em>(<em>f</em><em>l</em><em>a</em><em>g</em>) = 1<em>e</em><sup> − 12</sup></span> or one in one trillion.<em class="footnotelabel"></em></p>
<blockquote class="sidenote">
“The threshold is selected to provide an extremely low (1 in 1 trillion) probability of incorrectly flagging a given account.” - Apple Technical Summary
</blockquote>
<p>We can actually work backwards from that number to derive <span class="math inline"><em>P</em>(<code>falsepositive</code>)</span>:</p>
<p><br /><span class="math display">$$P(\texttt{flag}) = \sum_{\substack{x = t}}^T {T \choose x} \cdot P(\texttt{falsepositive})^x \cdot P(1-\texttt{falsepositive})^{T - x}  \approx 1\mathrm{e}^{-12}$$</span><br /></p>
<p>In order to finalize this we only need to make educated guesses about 2 parameters: the threshold value, <span class="math inline"><em>t</em></span>, and the total number of photos checked per year, <span class="math inline"><em>T</em></span>. Apple throws out the number <span class="math inline"><em>t</em> = 10</span> in their technical summary, which seems like a good place to start.</p>
<p>Assuming an average account generates 3-4 pictures a day to be checked then <span class="math inline"><em>T</em> ≈ 1278</span> over a year. Plugging in those numbers, and we get <span class="math inline"><em>P</em>(<code>falsepositive</code>) ≈ 0.00035</span> or <strong>1 in 2858</strong>.</p>
<p>Does that number have any relation to reality? There is evidence<em class="footnotelabel"></em> to suggest <span class="sidenote"><a href="https://arxiv.org/abs/2106.09820">Adversarial Detection Avoidance Attacks: Evaluating the robustness of perceptual hashing-based client-side scanning.</a> Shubham Jain, Ana-Maria Cretu, Yves-Alexandre de Montjoye</span> that the false acceptance rate for common perceptual hashing algorithms is between 0.001-0.01 for a database size of 500K.</p>
<p>That makes our guesstimate of 0.00035 an order of magnitude smaller than the most generous empirical estimate. We will be generous and assume Apple broke some new ground with NeuralHash and 0.00035 represents a major improvement in perceptual hashing false acceptance rates.</p>
<p>Given that we can go back and calculate the probability of observing, <span class="math inline"><em>P</em>(<code>match</code>)</span>, a match each day…</p>
<p><br /><span class="math display">$$P(\texttt{match}) = 1 - (( 1 - {0.00035})^{3.5}) \approx {0.001225} \approx \frac{1}{{816}}$$</span><br /></p>
<p>Or, a match once on average every 816 days for a person that only stores 3-4 photos per day.</p>
<p>Not everybody is every person though, if we applied the same <span class="math inline"><em>P</em>(<code>falsepositive</code>)</span> to a new parent who takes upwards of 50 photos per day, then their <span class="math inline"><em>P</em>(<code>match</code>)</span> is:</p>
<p><br /><span class="math display">$$P(\texttt{match}) = 1 - (( 1 - {0.00035})^{50}) \approx {0.01735}  \approx \frac{1}{{57}}$$</span><br /></p>
<p>Or, a match on average every 57 days.</p>
<p>At this point I feel compelled to point out that these are <strong>average</strong> match probabilities. For the prolific photo taking parent who takes 18250 photos a year, the probability that they actually exceed the threshold in false matches is 6%<em class="footnotelabel"></em> <span class="sidenote">assuming <span class="math inline"><em>t</em></span> is 10</span>.</p>
<p>It is also worth mentioning that even though we ballparked <span class="math inline"><em>t</em></span> and <span class="math inline"><em>T</em></span> there are explicit constraints on what their values can be. If Apple generates a single <span class="math inline"><em>t</em></span> for all accounts, then <span class="math inline"><em>T</em></span> needs to be an approximation on the average number of photos an account stores per year. If Apple generates a different <span class="math inline"><em>t</em></span> value for every account, then it has enough information already to derive <span class="math inline"><em>P</em>(<code>observation</code>)</span> and break its own obfuscation.</p>
<hr/>
<p>Using what we now know we can assess the server side operations show how the observer can calculate the probability of a real match given the probability of any q observation and the probability of a synthetic match.</p>
<h3 id="the-probability-of-synthetic-matches">The Probability of Synthetic Matches</h3>
<p>Before we go any further, we should address one particular complication. Apple appears to suggest that images are probabilistically replaced with synthetic matches:</p>
<blockquote>
“the device occasionally produces synthetic vouchers for images as opposed to ones corresponding to their image”
</blockquote>
<p>Given that, the rate of real matches v.s. synthetic matches isn’t independent. <strong>Actual matches might be replaced by synthetic matches</strong>.</p>
<p><br /><span class="math display">$$P(\texttt{observation}) = (P(\texttt{match}) \cdot (1 - P(\texttt{synthetic}))) + ((1-P(\texttt{match})) \cdot P(\texttt{synthetic})) \vphantom{+ \frac{1}{1}}$$</span><br /></p>
<p>Or, to put it another way, the probability of a match being reported as a match is dependent on the probability it isn’t reported as a synthetic. Either way, <span class="math inline"><em>P</em>(<code>observation</code>|<code>match</code>) = 1</span></p>
<p>Further we can actually make a guess at the value of <span class="math inline"><em>P</em>(<code>synthetic</code>)</span> under the assumption that it is calculated globally. Since Apple have stated that <span class="math inline"><em>P</em>(<code>synthetic</code>)</span> is dependent on <span class="math inline"><em>t</em></span> and is designed such that it generates synthetic matches in the same order or magnitude as <span class="math inline"><em>t</em></span> we can derive <span class="math inline"><em>P</em>(<code>synthetic</code>)</span> such that each device generates <span class="math inline"><em>t</em></span> synthetic matches a year on average.</p>
<p>Using our numbers from earlier we can place <span class="math inline"><em>P</em>(<code>synthetic</code>) ≈ 0.01</span> which would mean that over the course of a year, an average account storing 3-4 messages a day would have ~70% chance of generating 10 or more synthetic vouchers.</p>
<p>The exact value doesn’t really matter for our purposes. Any order of magnitude greater than <span class="math inline">0.01</span> results in too many synthetic matches, and any order of magnitude smaller results in too few.</p>
<h3 id="calculating-synthetic-probabilities">Calculating Synthetic Probabilities</h3>
<p>Given what we know about the probabilities in this system we can now piece together a server-side attribution attack to break the privacy provided by synthetic matches.</p>
<p><br /><span class="math display">$$P(\texttt{match}| \texttt{observation}) = \frac{P(\texttt{observation}|\texttt{match}) \times P(\texttt{match})}{P(\texttt{observation})}$$</span><br /></p>
<p>We know that all matches will result in an observation and so…</p>
<p><br /><span class="math display">$$P(\texttt{match}| \texttt{observation}) = \frac{1 \times P(\texttt{match})}{P(\texttt{observation})}$$</span><br /></p>
<p>Or more simply:</p>
<p><br /><span class="math display">$$P(\texttt{match}| \texttt{observation}) = \frac{P(\texttt{match})}{P(\texttt{observation})}$$</span><br /></p>
<p>Given that the probability of a synthetic match is defined by Apple, the only unknown in the system is the probability of a match.</p>
<p>We can now plug in our values from earlier. Remember, <strong>Apple has stated</strong> that the actual probability of observing <span class="math inline"><em>t</em></span> false positive matches, <span class="math inline"><em>P</em>(<code>flag</code>)</span>, is <strong>1 in a trillion</strong> and as such we have been able to derive approximate probabilities for false positives.</p>
<p>For an “average” account that stores 3-4 photos per day we know that <span class="math inline"><em>P</em>(<code>match</code>) =  ≈ 0.001225</span>, allowing Apple, who defines P() to calculate:</p>
<p><br /><span class="math display">$$P(\texttt{match}| \texttt{observation}) = \frac{(0.001225 \cdot 0.99)}{(0.001225 \cdot 0.99) + (0.998775 \cdot 0.01))} \approx 0.11 $$</span><br /></p>
<p>Given that we are aiming for ~10 synthetics over the course of a year, an 11% probability of any observation being a real match seems about the right level of indistinguishably.</p>
<p>But, what about our prolific “parent” account that stores 50 photos per day? We know that <span class="math inline"><em>P</em>(<code>match</code>) ≈ 0.01735</span>, allowing Apple, who defines <span class="math inline"><em>P</em>(<code>synthetic</code>)</span> to calculate:</p>
<p><br /><span class="math display">$$P(\texttt{match}| \texttt{observation}) = \frac{(0.01735 \cdot 0.99)}{(0.01735 \cdot 0.99) + (0.98265 \cdot 0.01))} \approx 0.63 $$</span><br /></p>
<p>That is a 63% probability that any reported match is a real match and not a synthetic one!</p>
If Apple define a global <span class="math inline"><em>P</em>(<code>synthetic</code>)</span> then different accounts will naturally have different server-side distributions of observations, and these can be used to tighten the estimates of true matches.<em class="footnotelabel"></em>
<p class="sidenote">
And, again, if Apple can define <span class="math inline"><em>P</em>(<code>synthetic</code>)</span> on a per-account basis then they have <strong>more</strong> information to use when tightening these estimates
</p>
<p>The secrecy of this metadata is then is <strong>paradoxically dependent</strong> on both Apple never deriving <span class="math inline"><em>P</em>(<code>observation</code>)</span> for themselves <em>and</em> on Apple generating a distinct <span class="math inline"><em>P</em>(<code>synthetic</code>)</span> for each account. Or rather, the privacy of one of the most sensitive aspects of this system requires Apple both collecting no information on accounts, and also on Apple knowing enough about accounts to derive the parameters necessary to keep the information private.</p>
<hr/>
<p>It is actually much worse than that though.</p>
<p>While priors may start as an unknown<em class="footnotelabel"></em> <span class="sidenote">(setting aside the fact that Apple already has enough data to derive this themselves)</span>, Apple quickly generates a large amount of data relating to when new observations are made. Since people are different in the ways these take and store photos and live in different parts of the world, the exact probability of them triggering a check is dependent on them.</p>
<p>There are also additional discriminating events in the system itself.</p>
<h3 id="matches-over-threshold-without-decryption">Matches over Threshold without Decryption</h3>
<p>One of the most obvious sources of discriminating information is built explicitly into the design. The threshold scheme as proposed requires <span class="math inline"><em>t</em></span> real matches in order to decrypt the inner envelopes containing the matches images.</p>
<p>As such. every time a new match is observed and <span class="math inline"><em>t</em>′</span> is over the threshold the system learns additional information regarding the makeup of previous matches.</p>
<p>To illustrate, if we set the <span class="math inline"><em>t</em> = 10</span>, then after the system observes <span class="math inline">10</span> matches (made up of a number of an unknown number of real or synthetic matches) then every additional match until decryption can be used to derive information about the previous matches. If the next match to arrive does not allow decryption then the server can derive that there are at least 2 synthetic matches in the bucket. This confidence increases with every observation that does not result in decryption.</p>
<p>Combining that with our bayesian estimates fom earlier we can see how an adversarial observer could update their estimates of <span class="math inline"><em>P</em>(<code>synthetic</code>)</span> based on the total number of observations, and the fact that no decryption has taken place.</p>
<hr/>
<h3 id="what-does-this-all-mean">What does this all mean?</h3>
<p>In this analysis we have deliberately left out other information that Apple, or someone who can compel Apple, may use to tighten these estimates e.g. derived from public social media feeds.</p>
<p>In reality, an adversary will have far greater access to auxiliary data about the environment and target sets than simply raw probabilities.</p>
<p>In that kind of environment, no amount of server-defined obfuscation is enough to protect the metadata that the server holds.</p>
<p>In this case that metadata is a rather controversial number i.e. <strong>the number of possible matches to illegal material detected on the device</strong>.</p>
<p>That is interesting metadata to countless entities including the law enforcement and intelligence agencies of multiple jurisdictions and states.</p>
<p>Even if we strictly limit the type of material that Apple is searching for, the high likelihood of false positive events combined with the ease at which Apple can likely distinguish true matching events from synthetic events<em class="footnotelabel"></em> <span class="sidenote"> (as worked through above)</span> should concern any potential subject of the system.</p>
<p><strong>Innocence is no defense against judgements made using derived metadata</strong>.</p>
</article>
<hr/>
<h2>
Recent Articles
</h2>
<p><em>2021-08-16</em> – <a href="ftpsi-parameters.html">Revisiting First Impressions: Apple, Parameters and Fuzzy Threshold PSI</a><br><em>2021-08-12</em> – <a href="a_closer_look_at_fuzzy_threshold_psi.html">A Closer Look at Fuzzy Threshold PSI (ftPSI-AD)</a><br><em>2021-08-10</em> – <a href="obfuscated_apples.html">Obfuscated Apples</a><br></p>
<footer>
	Sarah Jamie Lewis
</footer>
</body>
</html>
